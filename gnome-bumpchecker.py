#!/usr/bin/env python3
# Copyright John N. Laliberte <allanonjl@gentoo.org>
# Copyright Daniel Gryniewicz <dang@gentoo.org>
# LICENSE - GPL2

import os, sys

sys.path.append(os.path.abspath(os.path.join(__file__, "..", "modules")))

version = "0.0.2"

if __name__ == '__main__':
    import clioptions_module
    options = clioptions_module.Options()
    if options.options.release_number is None:
        options.parser.error("No revision")

    # generate 2 lists.
    # 1st list is the packages needed for a release ( from GNOME FTP )
    # 2nd list is the latest versions of the packages w.r.t. the major/minor of
    # the release ( from GNOME FTP )
    import gnome_module

    gnome = gnome_module.GNOME(options.get_arguments().nextrev)

    # Quick versions file parsing
    release_packages = gnome.generate_data_release()
    latest_packages = gnome.generate_data_individual(release_packages)

    # Old FTP scraping way:
    # release_packages, latest_packages = gnome.generate_data_ftp()

    # figure out what versions of these packages are in portage.
    # we need a list of package names to check for, so we choose
    # to use the release_packages list.
    import portage_module
    packages_in_portage = \
            portage_module.find_packages_in_tree(release_packages, \
                                        options.get_arguments().portdir, \
                                        options.get_arguments().all_overlays, \
                                        options.get_arguments().overlays, \
                                        options.options.stable)

    # compare the versions in order to check if we are up to date.
    comparison_result_packages = gnome_module.compare_packages(release_packages, \
                                                    latest_packages, \
                                                    packages_in_portage)

    # output these results to a nice html document
    import gnome_output
    gnome_output.Output(comparison_result_packages, True).generate_html()

    # if we specified to generate a keywords file, generate it
    # keep in mind this will do it for the versions in portage, which
    # may not be the release versions if we are not up to date.
    if options.get_arguments().keywords:
        gnome_output.Output(packages_in_portage, False).generate_keywords()
