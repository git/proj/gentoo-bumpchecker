#! /usr/bin/python2
# -*- coding: utf-8 -*-
# Copyright John N. Laliberte <allanonjl@gentoo.org>
# Copyright Daniel Gryniewicz <dang@gentoo.org>
# Copyright Tomas Chvatal <scarabeus@gentoo.org>
# LICENSE - GPL2

import os, sys

sys.path.append(os.path.abspath(os.path.join(__file__, "..", "modules")))

version = "0.0.1"

if __name__ == '__main__':

    import clioptions_module
    options = clioptions_module.Options()

    import xmodular_module

    x11 = xmodular_module.X_modular()

    # Quick versions file parsing
    release_packages, latest_packages = x11.generate_data_ftp()

    # figure out what versions of these packages are in portage.
    # we need a list of package names to check for, so we choose
    # to use the release_packages list.
    import portage_module
    packages_in_portage = \
            portage_module.find_packages_in_tree(release_packages, \
                                        options.get_arguments().portdir, \
                                        options.get_arguments().all_overlays, \
                                        options.get_arguments().overlays)

    stable_packages_in_portage = \
            portage_module.find_packages_in_tree(release_packages, \
                                        options.get_arguments().portdir, \
                                        options.get_arguments().all_overlays, \
                                        options.get_arguments().overlays, \
                                        True)
    #for pkg in stable_packages_in_portage:
    #     print "found stable: "+pkg.name+"-"+pkg.version

    # compare the versions in order to check if we are up to date.
    comparison_result_packages = xmodular_module.compare_packages(release_packages, \
                                                    latest_packages, \
                                                    packages_in_portage, \
                                                    stable_packages_in_portage)

    # output these results to a nice html document
    import xmodular_output
    xmodular_output.Output(comparison_result_packages, True).generate_html()

    # if we specified to generate a keywords file, generate it
    # keep in mind this will do it for the versions in portage, which
    # may not be the release versions if we are not up to date.
    if options.get_arguments().keywords:
        xmodular_output.Output(packages_in_portage, False).generate_keywords()
