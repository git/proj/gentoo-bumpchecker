# -*- coding: utf-8 -*-
# Copyright John N. Laliberte <allanonjl@gentoo.org>
# Copyright Ben de Groot <yngwin@gentoo.org>
# LICENSE - GPL2

import package_module, time, clioptions_module, gnome_module, os, cgi

class Output:

    def __init__(self, packages, to_calculate):
        self.packages = packages

        if to_calculate:
            self.calculate_stats()

    def calculate_stats(self):
        # variables to hold for stats
        total_packs = len(self.packages)
        self.update_needed = float(0)
        self.compliant = float(0)
        self.not_found = float(0)
        self.newer = float(0)

        for package in self.packages:
            if package.status == package_module.Status.Compliant:
                self.compliant += 1
            elif package.status == package_module.Status.NewerVersion:
                self.newer += 1
            elif package.status == package_module.Status.NotFound:
                self.not_found += 1
            elif package.status == package_module.Status.NeedUpdate:
                self.update_needed += 1

        try:
            self.update_percent = float(self.update_needed / total_packs) * 100
        except ZeroDivisionError:
            self.update_percent = 0
        try:
            self.compliant_percent = float(self.compliant / total_packs) * 100
        except ZeroDivisionError:
            self.compliant_percent = 0
        try:
            self.notfound_percent = float(self.not_found / total_packs) * 100
        except ZeroDivisionError:
            self.notfound_percent = 0
        try:
            self.total_percent = float( (self.update_needed + self.not_found ) / total_packs ) * 100
        except ZeroDivisionError:
            self.total_percent = 0

    def generate_html(self):

        # now we have all the results in the results list.
        # just time to generate some kind of "useful" output.
        # for now, lets just make a html file. ( using html5 and css )
        # name, portage_version, gnome_version, status <-- is whats in the PackageUpdate object
        current_time = str(time.asctime(time.localtime()))

        lines = []

        # header
        lines.append('<!DOCTYPE html>')
        lines.append("<html>")
        lines.append('<head>')
        lines.append('  <meta charset="utf-8">')
        lines.append('  <title>Xorg Progress Table</title>')
        lines.append('  <link rel="stylesheet" type="text/css" href="gbc.css">')
        lines.append('</head>')
        lines.append("<body>")
        lines.append("<h1>Xorg Progress Table</h1>")
        lines.append("<p>Contact " + cgi.escape(os.environ["ECHANGELOG_USER"]) + " if anything is not correct.</p>")
        lines.append("<p>Generated date: " + current_time + "</p>")
        # stats
        lines.append('<div id="stats">')
        lines.append("<p>Compliant Packages: " + str('%0.2f' % self.compliant_percent)+ "%" + "  Number = " + str(self.compliant) + "</p>")
        lines.append("<p>Packages that need to be updated: " + str('%0.2f' % self.update_percent)+ "%" + "  Number = " + str(self.update_needed) + "</p>")
        lines.append("<p>New Packages that need to be added: " + str('%0.2f' % self.notfound_percent)+ "%" + "  Number = " + str(self.not_found) + "</p>")
        lines.append("</div>")

        lines.append('<table>')
        lines.append('<tr>')
        lines.append('<th class="pn">Package Name</th><th>Stable</th><th>Testing</th><th>Official</th><th>Latest</th>')
        lines.append('</tr>')

        # data
        for package in self.packages:
            if package.status == package_module.Status.NeedUpdate:
                lines.append('<tr class="needupdate">') # "red"
            elif package.status == package_module.Status.StableNeedUpdate:
                lines.append('<tr class="stableneedupdate">') # "blue"
            elif package.status == package_module.Status.Compliant:
                lines.append('<tr class="compliant">') # "green"
            elif package.status == package_module.Status.NotFound:
                lines.append('<tr class="notfound">') # "grey"
            elif package.status == package_module.Status.NewerVersion:
                lines.append('<tr class="newerversion">') # "yellow"

            lines.append('<td class="pn">' + str(package.name) + "</td>")
            lines.append('<td>' + str(package.stable_version) + "</td>")
            lines.append("<td>" + str(package.portage_version) + "</td>")
            lines.append("<td>" + str(package.gnome_version) + "</td>")
            lines.append("<td>" + str(package.latest_version) + "</td>")

            lines.append("</tr>")

        lines.append("</table>")

        # footer
        lines.append("<p>Official Version: Latest stable Xorg released individual packages.</p>")
        lines.append("<p>Latest Version: Latest available Xorg released individual packages, including the snapshots.</p>")
        lines.append("</body></html>")

        self.write_file(lines, clioptions_module.Options().get_arguments().output)

        print "Generated html output."

    def generate_keywords(self):
        lines = []
        for package in self.packages:
            package_string = package.category + "/" + package.name + "-" + package.version
            # only append revision if its not -r0
            if "r0" != package.revision:
                package_string += "-" + package.revision

            lines.append("=" + package_string)

        self.write_file(lines, "package.keywords")

        print "Generate package.keywords output."

    def write_file(self, lines, filename):
        file = open(filename, "w")
        file.writelines(x +'\n' for x in lines)
        file.flush()
        file.close()
        del file
