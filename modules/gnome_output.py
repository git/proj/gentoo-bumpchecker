# Copyright John N. Laliberte <allanonjl@gentoo.org>
# LICENSE - GPL2

import package_module, time, clioptions_module, gnome_module, os

class Output:

    def __init__(self, packages, to_calculate):
        self.packages = packages

        if to_calculate:
            self.calculate_stats()

    def calculate_stats(self):
        # Variables for holding stats
        total_packs = len(self.packages)
        self.update_needed = 0
        self.compliant = 0
        self.not_found = 0
        self.newer = 0
        # Percentages
        self.update_percent = 0
        self.notfound_percent = 0
        self.compliant_percent = 0

        for package in self.packages:
            if package.status == package_module.Status.Compliant:
                self.compliant += 1
            elif package.status == package_module.Status.NewerVersion:
                self.newer += 1
            elif package.status == package_module.Status.NotFound:
                self.not_found += 1
            elif package.status == package_module.Status.NeedUpdate:
                self.update_needed += 1

        if total_packs == 0:
            return

        self.update_percent = 100 * self.update_needed / float(total_packs)
        self.compliant_percent = 100 * self.compliant / float(total_packs)
        self.notfound_percent = 100 * self.not_found / float(total_packs)

    def generate_html(self):

        # now we have all the results in the results list.
        # just time to generate some kind of "useful" output.
        # for now, lets just make a crappy html file. ( this should use css and the like )
        # name, portage_version, gnome_version, status <-- is whats in the PackageUpdate object
        current_time = str(time.asctime(time.gmtime()))

        lines = []

        # header
        lines.append('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">')
        lines.append("<html>")
        lines.append('<head>')
        lines.append('<title>Gnome ' + clioptions_module.Options().get_arguments().release_number + (' Stable' if clioptions_module.Options().options.stable else '') + ' Progress</title>')
        lines.append('<link rel="stylesheet" type="text/css" href="default.css"></link>')
        lines.append('</head>')
        lines.append("<body>")
        lines.append("<h2>Gnome " + clioptions_module.Options().get_arguments().release_number + (' Stable' if clioptions_module.Options().options.stable else '') + " Progress</h2>")
        lines.append("contact " + os.environ["USER"] + "@gentoo.org if anything is not correct<br>")
        lines.append("Generated UTC date: " + current_time + "<br>")

        # stats
        lines.append("<br>")
        lines.append("Compliant Packages: %d (%0.2f%%)<br />" % (self.compliant, self.compliant_percent))
        lines.append("Packages requiring update: %d (%0.2f%%)<br />" % (self.update_needed, self.update_percent))
        lines.append("Packages missing from tree: %d (%0.2f%%)<br />" % (self.not_found, self.notfound_percent))
        lines.append("<br>")

        lines.append('<table cellpadding="3">')
        lines.append('<tr>')
        lines.append("<th>Package Name</th><th>Gentoo Version</th><th>Official Version</th><th>Latest Version</th>")
        lines.append('</tr>')

        current_suite = None
        # data
        for package in self.packages:
            if package.suite != current_suite:
                current_suite = package.suite
                lines.append('<tr><td colspan="4" align="center"><b>%s</b></td></tr>' % current_suite.capitalize())
            if package.status == package_module.Status.NeedUpdate:
                lines.append('<tr bgcolor="#FF8A75">') # "red"
            elif package.status == package_module.Status.Compliant:
                lines.append('<tr bgcolor="#D0EE80">') # "green"
            elif package.status == package_module.Status.NotFound:
                lines.append('<tr bgcolor="#EBEBEB">') # "grey"
            elif package.status == package_module.Status.NewerVersion:
                lines.append('<tr bgcolor="#FFFF95">') # "yellow"

            lines.append("<td>" + str(package.name) + "</td>")
            lines.append("<td>" + str(package.portage_version) + "</td>")
            lines.append("<td>" + str(package.gnome_version) + "</td>")
            lines.append("<td>" + str(package.latest_version) + "</td>")

            lines.append("</tr>")

        lines.append("</table>")

        # footer
        lines.append("<br>Official Version: GNOME release teams blessed version for the whole GNOME-%s set" % clioptions_module.Options().get_arguments().release_number)
        lines.append("<br>Latest Version: Latest available version release of the same GNOME release cycle")
        lines.append("</html>")

        self.write_file(lines, clioptions_module.Options().get_arguments().output)

        print("Generated html output.")

    def generate_keywords(self):
        lines = []
        for package in self.packages:
            package_string = package.category + "/" + package.name + "-" + package.version
            # only append revision if its not -r0
            if "r0" != package.revision:
                package_string += "-" + package.revision

            lines.append("=" + package_string)

        self.write_file(lines, "package.keywords")

        print("Generate package.keywords output.")

    def write_file(self, lines, filename):
        file = open(filename, "w")
        file.writelines(x +'\n' for x in lines)
        file.flush()
        file.close()
        del file
