# -*- coding: utf-8 -*-
# Copyright Tomas Chvatal <scarabeus@gentoo.org>
# LICENSE - GPL2

# X11 module
import urllib2, package_module, string, simple_cache_module
import clioptions_module
import ftp_module

DEBUG = False
class X_modular:
    def __init__(self):
        options = clioptions_module.Options()
        args = options.get_arguments()
        self.ftpserver = "ftp.x.org"
        #self.release_directories = [ "pub/current/src/app/", "pub/current/src/data/", "pub/current/src/doc/",
        #    "pub/current/src/driver/", "pub/current/src/font/", "pub/current/src/lib/", "pub/current/src/proto/",
        #    "pub/current/src/util/", "pub/current/src/xserver/" ]
        self.latest_directories = [ "pub/individual/app/", "pub/individual/data/", "pub/individual/doc/",
            "pub/individual/driver/", "pub/individual/font/", "pub/individual/lib/", "pub/individual/proto/",
            "pub/individual/util/", "pub/individual/xserver/" ]

    def generate_data_ftp(self):
        walker = ftp_module.FTPWalker(self.ftpserver, "anonymous", "test@test.com")
        #files = []
        #for directory in self.release_directories:
        #    f_files = ftp_module.find_files(walker, directory, "", "")
        #    files.extend(f_files)
        # remove everything expect the tar.bz2
        #files = self.filter_files(files)

        individual_files = []
        for directory in self.latest_directories:
            #print "working in: "+directory
            f_files = ftp_module.find_files(walker, directory, "", "")
            individual_files.extend(f_files)
        individual_files = self.filter_files(individual_files)

        # create package objects for the files
        #release_packages = [] # the packages that are required for a release
        #for package_name in files:
        #    release_package = package_module.Package(package_name)
        #    release_packages.append(release_package)

        individual_packages = []
        for individual_package_name in individual_files:
            individual_package = package_module.Package(individual_package_name)
            individual_packages.append(individual_package)

        # searching for formated packages
        #for ip in individual_packages:
        #    if ip.name=="xkeyboard-config":
        #        print ip.name+"-"+ip.version

        latest_packages = [] # the latest versions
        package_in_use = [] # checker so we dont repeat packages
        for package in individual_packages:
            # i am lazy so lets do just one way sort. Should be rewritten into something faster :]
            used = "false"
            for packname in package_in_use:
                if packname == package.name:
                    used = "true"
                    break
            if used == "true":
                continue

            package_in_use.append(package.name)
            latest = self.get_latest(individual_packages, package.name, package.version)

            latest_packages.append(package_module.Package(package.name + "-" + latest))

        snapshot_packages = []
        for package in latest_packages:
            latest = self.get_latest(individual_packages, package.name, package.version, "true")
            snapshot_packages.append(package_module.Package(package.name + "-" + latest))

        return (latest_packages, snapshot_packages)

    def get_latest(self, rel_packages, name, version, snapshots="false"):
        length = len(version.split("."))
        major = int(version.split(".")[0])
        minor = int(version.split(".")[1])
        if length < 3:
            latest = str(major)+"."+str(minor)
            subminor = 0
        else:
            subminor = int(version.split(".")[2]) # usually the lovely .99.
        if length < 4:
            latest = str(major)+"."+str(minor)+"."+str(subminor)
            subsubminor = 0
        else:
            subsubminor = int(version.split(".")[3])
        if length >= 4:
            latest = str(major)+"."+str(minor)+"."+str(subminor)+"."+str(subsubminor)
        #rel_packages.sort()
        for paclist in rel_packages:
            if name == paclist.name: # we have the correct package name
                length = len(paclist.version.split("."))
                loc_major = int(paclist.version.split(".")[0])
                loc_minor = int(paclist.version.split(".")[1])
                if length < 3:
                    loc_subminor = 0
                else:
                    loc_subminor = int(paclist.version.split(".")[2])
                if length < 4:
                    loc_subsubminor = 0
                else:
                    loc_subsubminor = int(paclist.version.split(".")[3])

                if snapshots == "false" and ( ( loc_major > 98 or loc_minor > 98 or loc_subminor > 98 or loc_subsubminor > 98 ) or ( name == "pixman" and  self.is_prvocislo(loc_minor) ) ):
                    continue
                # Debuging why some package does not show correct version...
                #if name == "xkeyboard-config":
                #    print "Vychozi: "+str(major)+"."+str(minor)+"."+str(subminor)+"."+str(subsubminor)
                #    print "Lokalni: "+str(loc_major)+"."+str(loc_minor)+"."+str(loc_subminor)+"."+str(loc_subsubminor)
                if loc_major < major:
                    continue
                if loc_major == major and loc_minor < minor:
                    continue
                if loc_major == major and loc_minor == minor and loc_subminor < subminor:
                    continue
                if loc_major == major and loc_minor == minor and loc_subminor == subminor and loc_subsubminor < subsubminor:
                    continue

                major = loc_major
                minor = loc_minor
                subminor = loc_subminor
                subsubminor = loc_subsubminor
                if length < 3:
                    latest = str(major)+"."+str(minor)
                elif length < 4:
                    latest = str(major)+"."+str(minor)+"."+str(subminor)
                else:
                    latest = str(major)+"."+str(minor)+"."+str(subminor)+"."+str(subsubminor)
            else:
                continue
        return latest

    def is_prvocislo(self, number):
        #print number
        if number % 2 == 1:
            return True
        else:
            return False

    def filter_files(self, files):
        # we want to filter out all the bad files.
        newfiles = []
        for file in files:
            # only keep files with .tar.bz2 or .tar.gz ending.
            if ( ( ( 0 < file.find(".tar.") and 0 < file.find(".bz2") ) or ( 0 < file.find(".tar.") and 0 < file.find(".gz") ) ) and 0 > file.find(".asc") and 0 > file.find(".sha") ):
                file = string.replace(file, ".tar.gz", "")
                file = string.replace(file, ".tar.bz2", "")
                newfiles.append(file)

        return newfiles

import portage_module
def compare_packages(release_packages, latest_packages, packages_in_portage, stable_packages_in_portage):
    # we care about 5 cases
    # 1.  portage version is less than the release version. (RED)
    # 2.  portage version is equal to the release version, but less than the latest version. (LIGHT GREEN)
    # 3.  portage version is equal to the release version and the latest version. (GREEN)
    # 4.  portage version is greater than the release version (GREEN)
    # 5.  package does not exist in portage (GREY)

    # again, we choose release_packages as the enumerator for the package names
    # since it will have ALL packages ( for example, if we used portage_packages, we
    # might miss the packages that do not exist in portage )
    status_packages = []
    for package in release_packages:
        color = None
        release_package = package
        latest_package = portage_module.findpackage(package.name, latest_packages)
        portage_package = portage_module.findpackage(package.name, packages_in_portage)
        stable_portage_package = portage_module.findpackage(package.name, stable_packages_in_portage)

        if stable_portage_package is None and portage_package is None:
            status = package_module.Status.NotFound
            stable_portage_package = package_module.Package(package.name)
            portage_package = package_module.Package(package.name)
        elif stable_portage_package is None and portage_package is not None:
            status = package_module.Status.StableNeedUpdate
            stable_portage_package = package_module.Package(package.name)
        elif portage_package is None:
            status = package_module.Status.NotFound
            portage_package = package_module.Package(package.name)
        elif portage_module.best_version_test(portage_package.name_plus_version, \
                                     release_package.name_plus_version) == 2:
            status = package_module.Status.NeedUpdate
        elif portage_module.best_version_test(portage_package.name_plus_version, \
                                       latest_package.name_plus_version) == 2:
            status = package_module.Status.NewerVersion
        elif portage_module.best_version_test(stable_portage_package.name_plus_version, \
                                       portage_package.name_plus_version) == 2:
            status = package_module.Status.StableNeedUpdate
        else:
            status = package_module.Status.Compliant

        #if portage_package is not None:

        # FIXME: Reports release version as latest version to not have to deal with this right now
        if latest_package is None:
            print "No latest version known for %s, FIXME!" % release_package.name
            latest_package = release_package

        if DEBUG:
            print "package: " + str(release_package.name) + \
                 " | sp: " + str(stable_portage_package.version) + \
                 " | pp: " + str(portage_package.version) + \
                 " | rp: " + str(release_package.version) + \
                 " | lp: " + str(latest_package.version) + \
                 " | status: " + str(status)

        status_packages.append(package_module.PackageStatus(release_package.name, str(portage_package.version), str(release_package.version), str(latest_package.version), status, str(stable_portage_package.version)))

    return status_packages
