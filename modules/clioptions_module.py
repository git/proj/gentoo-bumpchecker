# Copyright John N. Laliberte <allanonjl@gentoo.org>
# LICENSE - GPL2

# command line options module.
from optparse import OptionParser

class Options:

    def __init__(self):
        usage = "usage: %prog [-qkn] -o <filename> -r <release_number>"
        self.parser = OptionParser(usage=usage)
        self.setup_parser()

        self.options = self.parser.parse_args()[0]

    def setup_parser(self):
        self.parser.add_option("-o", "--output", dest="output",
                       help="write report to FILE", metavar="FILE")
        self.parser.add_option("-q", "--quiet",
                           action="store_false", dest="verbose", default=True,
                           help="don't print status messages to stdout")
        self.parser.add_option("-r", "--release", dest="release_number",
                           help="release version to check for on ftp")
        self.parser.add_option("-k", "--keywords",
                           action="store_true", dest="keywords", default=False,
                           help="Generate a package.keywords file for the versions found in portage.")
        self.parser.add_option("-n", "--nextrev",
                           action="store_true", dest="nextrev", default=False,
                           help="Check for a minor revision newer than the given one")
        self.parser.add_option("-p", "--portdir",
                           dest="portdir", default=None,
                           help="Specify an alternate tree to use as PORTDIR")
        self.parser.add_option("-A", "--all-overlays",
                           action="store_true", dest="all_overlays",
                           default=False,
                           help="Scan all available overlays in addition to PORTDIR.")
        self.parser.add_option("-a", "--overlay", metavar="OVERLAY",
                           action="append", dest="overlays", default=None,
                           help="Overlay to scan in addition to PORTDIR. Specify multiple times to scan more than one overlay.")
        self.parser.add_option("-S", "--stable",
                           action="store_true", dest="stable", default=False,
                           help="Force considering stable versions as latest available in Gentoo for the GNOME module.")

    def get_arguments(self):
        if self.options.output is None:
            self.parser.error("No output file")
        return self.options
