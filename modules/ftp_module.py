# Copyright John N. Laliberte <allanonjl@gentoo.org>
# LICENSE - GPL2

#ftp module
# large amount of this taken from
# added in debug statements
# http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/436211

################
# my ftp module that should be easier to understand.
import os, ftplib

DEBUG = False
class FTPWalker:
    def __init__(self, site, user, passwd):
        self.ftp = ftplib.FTP(site, user, passwd)
        if DEBUG: print("logged into ftp")

    def cd(self, path):
        try:
            self.ftp.cwd(path)
            #sys.stdout.write(".")
            if DEBUG: print("successfully changed path to " + path)
            return True
        except:
            print("Directory does not exist " + path)
            return False

    def pwd(self):
        return self.ftp.pwd()

    def ls(self, cwd):
        lines = []
        self.ftp.retrlines("LIST", lines.append)
        return map(lambda x: self.extract_info(cwd, x), lines)

    def str2perm(self, str):
        return str[0] == 'd', str[0] == 'l'

    def extract_info( self, cwd, line ):
        fullmode, links, owner, group, size, rest = line.split(None, 5)
        isdir, islink = self.str2perm(fullmode)
        name = rest[13:]
        #if 0 < string.find(name,"->"):
        if islink:
            name, symbolic = name.split("->")
            name = name.strip()
            symbolic = symbolic.strip()
        if DEBUG: print("Name of the file is: " + name)

        return FileInformation(name, isdir)

class FileInformation:
    def __init__(self, name, isdirectory):
        self.name = name
        self.isdir = isdirectory

def pattern(p, v):
    return fnmatch.fnmatch(v, p)

def dropslashes( str ):
    i, n = 0, len( str )
    while i < n and str[i] == '/': i += 1
    return str[i:]

def excluded(exclude_patterns, dir):
    for exclude_pattern in exclude_patterns:
        if pattern(exclude_pattern, dir):
            return True
    return False

def listSiteGen(walker, dir, excluded_names):
    path = walker.pwd()
    #if not excluded( excluded_names, dir ) and walker.cd( dir ):
    if walker.cd(dir):
        newpath = dropslashes(os.path.join(path, dir))
        lsresult = walker.ls(newpath)
        for info in lsresult:
            if info.isdir:
                for rec_info in listSiteGen(walker, info.name, excluded_names):
                    yield rec_info
            else:
                yield info
    walker.cd(path)

def find_files(walker, dir, excluded_names, excluded_suffixes):
    file_listing = []

    for fileinfo in listSiteGen(walker, dir, excluded_names):
        #if not excluded( excluded_suffixes, fileinfo):
        if DEBUG: print("Appending: " + str(fileinfo.name))
        file_listing.append(fileinfo.name)

    return file_listing
