# Copyright John N. Laliberte <allanonjl@gentoo.org>
# LICENSE - GPL2

# simple cache module

# This module will store information about the packages we read off of the gnome ftp.
# This way we don't have to constantly contact the GNOME ftp during the development process
# of this program.

class SimpleCache:

    def __init__(self):
        self.filename = "cache.txt"
        self.write_queue = []

    def write_to_queue(self, release_version, latest_version):
        self.write_queue.append(release_version + "," + latest_version)

    def append(self, list_of_lines):
        try:
            # open file stream
            file = open(self.filename, "a")
        except IOError:
            print "There was an error writing to"+self.filename
            sys.exit()

        for line in list_of_lines:
            file.write(line+"\n")

        file.close()

    def flush_queue(self):
        self.append(self.write_queue)


class FileStuff:
    def __init__(self, filename):
        self.filename = filename
        self.lines = []

    def read(self):
        file = self.open("r")
        # read the file in line by line, and then return it
        for line in file.readlines():
            # replace the newline characters
            line = string.replace(line, '\n', '')
            # add it to the collection
            self.lines.append(line)
        file.close()

        return self.lines

    def write(self, list_of_lines):
        try:
            # open file stream
            file = self.open("w")
        except IOError:
            print "There was an error writing to"+self.filename
            sys.exit()

        for line in list_of_lines:
            file.write(line+"\n")

        file.close()

    def append(self, list_of_lines):
        try:
            # open file stream
            file = self.open("a")
        except IOError:
            print "There was an error writing to"+self.filename
            sys.exit()

        for line in list_of_lines:
            file.write(line+"\n")

        file.close()

    def open(self, type):
        try:
            file = open(self.filename, type)
            return file
        except IOError:
            print "Error reading/writing " + type
            sys.exit()
