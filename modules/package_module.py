# -*- coding: utf-8 -*-
# Copyright John N. Laliberte <allanonjl@gentoo.org>
# LICENSE - GPL2

# the package module
import sys
sys.path = ["modules"]+sys.path
import portage_module

class Package(object):
    special_cases = {
        "cantarell-fonts":"cantarell",
        "gfbgraph":"libgfbgraph",
        "GConf":"gnome-base/gconf",
        "ORBit2":"orbit",
        "libidl":"libIDL",
        "libxml++":"libxmlpp",
        "pkg-config":"pkgconfig",
        "glade":"dev-util/glade",
        "glade3":"dev-util/glade",
        "gdl":"dev-libs/gdl",
        "network-manager-applet": "nm-applet",
        "NetworkManager": "networkmanager",
        "gtk-theme-engine-clearlooks": "gtk-engines-clearlooks",
        "gom":"dev-libs/gom",
        "gnome-themes-extra":"gnome-themes-standard",
        "gtksourceview":"gui-libs/gtksourceview",
        # Perl package mappings
        'Glib'          : 'dev-perl/glib-perl',
        'Gnome2-Canvas' : 'dev-perl/gnome2-canvas',
        'Gnome2-GConf'  : 'dev-perl/gnome2-gconf',
        'Gnome2-VFS'    : 'dev-perl/gnome2-vfs-perl',
        'Gnome2'        : 'dev-perl/gnome2-perl',
        'Gtk2-GladeXML' : 'dev-perl/gtk2-gladexml',
        'Gtk2'          : 'dev-perl/gtk2-perl',
        'Pango'         : 'dev-perl/Pango',
        # Haskell mess
        "glib":"dev-libs/glib",
        "gtk":"gui-libs/gtk",
        "pango":"x11-libs/pango",
        "gstreamer":"media-libs/gstreamer",
        "vte":"x11-libs/vte",
    }

    def __init__(self, raw_data, suite=None):
        self.raw_string = raw_data
        self.suite = suite

        self.name = None
        self.version = None
        self.revision = None
        self.major_minor = None
        self.raw_name = None

        self.package_version = None

        self.category = None

        self.parse_raw_string(raw_data)

        self.handle_special_cases()

    @property
    def name_plus_version(self):
        return self.name + "-" + self.version

    @property
    def name_plus_version_plus_revision(self):
        return self.name_plus_version + "-" + self.revision

    def handle_special_cases(self):
        if self.name in self.special_cases:
            self.name = self.special_cases[self.name]

    def parse_raw_string(self, raw_string):
        split_string = portage_module.split_package_into_parts(raw_string)
        if None != split_string:
            #[cat, pkgname, version, rev ]
            self.category, self.name, self.version, self.revision = split_string
            self.raw_name = self.name
            self.major_minor = self.parse_mm(self.version)
#        else:
#            print("Error, " + raw_string + " is not a valid package!")

    def parse_mm(self, version):
        return '.'.join(version.split('.')[0:2])

    @property
    def major(self):
        return self.major_minor.split('.')[0]

    @property
    def minor(self):
        return self.major_minor.split('.')[1] if self.major_minor.find('.') != -1 else 'NONE' # TODO: workaround for gudev, refactor

    def print_info(self):
        print("Name: " + str(self.name))
        print("Version: " + str(self.package_version))
        print("Name+Version: " + str(self.name_plus_version))
        print("Raw: " + str(self.raw_string))

    def __repr__(self):
        return self.name_plus_version

class PackageStatus:
    def __init__(self, name, portage_version, gnome_version, latest_version, status, stable_version = False, suite=None):
        self.name = name
        if stable_version is not False:
            self.stable_version = stable_version
        self.portage_version = portage_version
        self.gnome_version = gnome_version
        self.latest_version = latest_version
        self.status = status
        self.suite = suite

class Status(object):
    def Compliant(self):
        return 0
    def NeedUpdate(self):
        return 1
    def NotFound(self):
        return -1
    def NewerVersion(self):
        return 2
    def StableNeedUpdate(self):
        return 3

    property(Compliant)
    property(NeedUpdate)
    property(NotFound)
    property(NewerVersion)
    property(StableNeedUpdate)
