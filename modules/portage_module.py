# -*- coding: utf-8 -*-
# Copyright John N. Laliberte <allanonjl@gentoo.org>
# LICENSE - GPL2

# the portage module.
import os, sys
import package_module

import portage
import portage.util
import clioptions_module

sys.path = [portage.PORTAGE_PYM_PATH] + sys.path
os.environ["PORTAGE_CALLER"] = "depchecker"

def get_dbtree():
    return "porttree"

# given a package name in the form category/packagename-version-revision
# split it up into the different parts.
def split_package_into_parts(package_name):
    return portage.catpkgsplit(package_name)

# given a package name, find the category in portage.
# This will return None if you do not give it a version.
def find_category(package_name):
    package = portage.cpv_expand(package_name, mydb=portage.db["/"][get_dbtree()].dbapi, use_cache=1, settings=portage.settings)

    # catsplit returns ['category', 'packagename']
    package = portage.catsplit(package)
    return package[0]

# return 'pkg' for a package name of form 'pkg' or 'cat/pkg'
def get_pkgname(package):
    return portage.catsplit(package)[-1]

# given category/packagename-version-revision, find whether it is masked or not
def isMasked():
    pass

# compare packages to see which one is the best version
def best_version(package_list):
    return portage.best(package_list)

# find a given package in a list of packages (assuming package may be of the
# form 'pkg' or 'cat/pkg')
def findpackage(name, packages):
    for package in packages:
        if package.name == name or package.name == get_pkgname(name):
            return package

    return None

# which package is the higher version?
# returns 0 for equal
# 1 for package1
# 2 for package2
def best_version_test(package1, package2):
    best_package = portage.best([package1, package2])

    if best_package == package1 and best_package == package2:
        return 0
    elif best_package == package1:
        return 1
    else:
        return 2

# grabbing this from portageq, best to put it in here instead of
# having the overhead of calling it each time and getting the output.
def find_latest_package_in_tree(package_name, portdbapi=None):
    try:
        mylist = portdbapi.match(package_name)
        return portage.best(mylist)
    except KeyError:
        return None

# finds the latest available version of a package
def find_packages_in_tree(package_list, portdir=None, all_overlays=False, overlay_list=None, stable=False):
    # this value needs to be configurable from cl
    #overlays = ["/home/allanon/cvs/gnome"]

    portage_versions = []
    overlays = portage.settings['PORTDIR_OVERLAY']

    if all_overlays is False:
        overlays = ''
        if overlay_list is not None:
            overlays = overlay_list

    if stable is not False:
        # stable
        keywords = 'amd64' # the arch I care for :]
    else:
        #testing
        keywords = 'amd64 ~amd64' # the arch I care for :]

    if portdir is not None:
        mysettings = portage.config(local_config=False, env={'PORTDIR_OVERLAY': overlays, 'ACCEPT_KEYWORDS': keywords, 'PORTDIR': portdir})
    else:
        mysettings = portage.config(local_config=False, env={'PORTDIR_OVERLAY': overlays, 'ACCEPT_KEYWORDS': keywords})

    # hack because the above did not work :/
    mysettings['ACCEPT_KEYWORDS'] = keywords
    portdbapi = portage.portdbapi(mysettings=mysettings)

    for package in package_list:
        best_package = find_latest_package_in_tree(package.name, portdbapi)

        # if it returns a package, hardcode to first result.
        # cover the case where it doesn't exist in the tree.
        if None != best_package and "" != best_package:
            # Need to account for slotted packages here
            portage_versions.append(package_module.Package(best_package))

    return portage_versions

def tests():
    print(split_package_into_parts("media-gfx/gimp-3.5.0-r3"))
    print(split_package_into_parts("media-gfx/gimp-3.5"))
    print(find_category("gimp"))
    print(find_category("gimp-2.0"))
    print(best_version(["gimp-2.0", "gimp-2.0-r1", "gimp-3.0"]))
